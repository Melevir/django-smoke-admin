import django

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
        },
    }

SECRET_KEY = '_'
INSTALLED_APPS = (# required for admin
                  'django.contrib.auth',
                  'django.contrib.contenttypes',
                  'django.contrib.admin',
                  'django.contrib.sessions',

                  'django-smoke-admin',
#                  'test_app',
#                  'tests_intergational'
    )
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = ['--nocapture',]
ROOT_URLCONF = 'test_app.urls'
