from django.core.management.base import BaseCommand
import subprocess


class Command(BaseCommand):

    def handle(self, *args, **options):
        subprocess.call(['./test_app/runtests.py', 'django-smoke-admin'])
