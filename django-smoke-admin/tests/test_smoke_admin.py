# -*- coding: utf-8 -*-
from django.test import TestCase, TransactionTestCase
from django.contrib.auth.models import User
from django.contrib import admin
from django.core.urlresolvers import reverse
from django_dynamic_fixture import G


class SmokeAdminTest(TestCase):
    def setUp(self):
        User.objects.create_superuser('John', 'myemail@test.com', 'pwd')
        self.client.login(username='John', password='pwd')


class SmokeAdminBasicPagesTest(SmokeAdminTest):
    """Smoke tests for admin pages not related to models"""

    def test_index_page_avalible(self):
        """Index page responds correctly"""
        response = self.client.get(reverse('admin:index'))
        self.assertEqual(response.status_code, 200)

    def test_logout_page_avalible(self):
        """Logout page responds correctly"""
        response = self.client.get(reverse('admin:logout'))
        self.assertEqual(response.status_code, 200)

    def test_password_change_page_avalible(self):
        """Password change page responds correctly"""
        response = self.client.get(reverse('admin:password_change'))
        self.assertEqual(response.status_code, 200)

    def test_password_change_done_page_avalible(self):
        """Password change done page responds correctly"""
        response = self.client.get(reverse('admin:password_change_done'))
        self.assertEqual(response.status_code, 200)


class SmokeAdminModelSpecificPagesTest(SmokeAdminTest):
    """Smoke tests for admin pages for all registered models"""

    def setUp(self):
        super(SmokeAdminModelSpecificPagesTest, self).setUp()
        admin.autodiscover()
        self.registered_models = [m[0] for m in admin.site._registry.items()]
        self.model_objects = dict()
        for model in self.registered_models:
            self.model_objects[model] = G(model)

    def test_changelist_pages_avalible(self):
        """List pages of all registered models responds correctly"""
        for model in self.registered_models:
            response = self.client.get(reverse('admin:%s_%s_changelist'%(model._meta.app_label, model._meta.module_name)))
            self.assertEqual(response.status_code, 200)

    def test_add_pages_avalible(self):
        """Add pages of all registered models responds correctly"""
        for model in self.registered_models:
            response = self.client.get(reverse('admin:%s_%s_add'%(model._meta.app_label, model._meta.module_name)))
            self.assertEqual(response.status_code, 200)

    def test_change_pages_avalible(self):
        """Change pages of all registered models responds correctly"""
        for model in self.registered_models:
            response = self.client.get(reverse('admin:%s_%s_change'%(model._meta.app_label, model._meta.module_name),
                                       args=(self.model_objects[model].id,)))
            self.assertEqual(response.status_code, 200)

    def test_history_pages_avalible(self):
        """History pages of all registered models responds correctly"""
        for model in self.registered_models:
            response = self.client.get(reverse('admin:%s_%s_history'%(model._meta.app_label, model._meta.module_name),
                                       args=(self.model_objects[model].id,)))
            self.assertEqual(response.status_code, 200)

    def test_delete_pages_avalible(self):
        """Delete pages of all registered models responds correctly"""
        for model in self.registered_models:
            response = self.client.get(reverse('admin:%s_%s_delete'%(model._meta.app_label, model._meta.module_name),
                                       args=(self.model_objects[model].id,)))
            self.assertEqual(response.status_code, 200)
